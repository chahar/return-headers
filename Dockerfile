FROM node:14-alpine3.10
ENV APPDIR /usr/app
ENV NODE_ENV production
RUN mkdir -p $APPDIR
RUN npm install pm2 -g
WORKDIR $APPDIR
COPY . $APPDIR
CMD ["pm2-runtime", "./server.js"]
EXPOSE 8080

