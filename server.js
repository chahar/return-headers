const http = require('http');

const requestListener = function (req, res) {
  res.writeHead(200);
  console.log(JSON.stringify(req.headers));
  res.end(JSON.stringify(req.headers));
}

const server = http.createServer(requestListener);
server.listen(8080);

